from __future__ import unicode_literals
import frappe
from frappe import _

def update_price_list(self,method):
		price_list_name = frappe.db.sql("""select name
						from `tabItem Price` a 
						where a.item_code = %s and a.selling = 1 
						and a.creation = (select max(b.creation) from `tabItem Price` b 
			 			where a.item_code = b.item_code and b.selling = 1)""",self.item_code)
		if price_list_name:
			name = price_list_name[0][0]
			price_doc = frappe.get_doc("Item Price",name)
			price_doc.price_list_rate = self.standard_rate
			price_doc.save()

def update_item_price_list(self, method):
	for d in self.items:
		if d.x_selling_price != 0:
			price_list_name = frappe.db.sql("""select name
			 		from `tabItem Price` a 
					where a.item_code = %s and a.selling = 1 
					and a.uom = %s
			 		""",(d.item_code, d.uom))
			name = price_list_name[0][0]
			price_doc = frappe.get_doc("Item Price",name)
			price_doc.price_list_rate = d.x_selling_price
			price_doc.save()
			item_doc = frappe.get_doc("Item", d.item_code)
			item_doc.standard_rate = d.x_selling_price
			if item_doc.valuation_rate == 0:
				frappe.throw(_("Item Valuation rate in Item master cannot be zero"))
			factor = round((d.x_selling_price / item_doc.valuation_rate),2)
			if factor == 100:
				frappe.throw(__("Margin cannot be 100%"))
			calc_margin = round((100 - (100 / factor)),2)
			item_doc.x_margin = calc_margin
			item_doc.save()

def validate_purchase_rate(self, method):
	for d in self.items:
		if d.rate != 0 :
			if d.rate > d.last_purchase_rate:
				frappe.msgprint(_("Current purchase rate is greater than last purchase rate for item {0}").format(d.item_code))

def update_group_price(self,method):
	if self.is_group == 0:
		if self.x_margin >= 100:
			frappe.throw(_("Margin cannot be greater than or equal to 100"))
		item_name = frappe.db.sql("""select name as item_code,last_purchase_rate as last_purchase,
					valuation_rate as valuation_rate
					from `tabItem` a 
					where a.item_group = %s""",self.name,as_dict=1)

		for d in item_name:
			price_list_name = frappe.db.sql("""select name
			 		from `tabItem Price` a 
			 		where a.item_code = %s and a.selling = 1 
			 		and a.creation = (select max(b.creation) from `tabItem Price` b 
			 		where a.item_code = b.item_code and b.selling = 1)""",d["item_code"])
			if price_list_name:
				name = price_list_name[0][0]
				selling_price = round((d["valuation_rate"] or d["purchase_rate"]) / (1 - (self.x_margin/100)),2)
				price_doc = frappe.get_doc("Item Price",name)
				price_doc.price_list_rate = selling_price
				price_doc.save()
				item_doc = frappe.get_doc("Item", d["item_code"])
				if item_doc.valuation_rate == 0:
					frappe.throw(_("Item Valuation rate in Item master cannot be zero"))
				factor = round((selling_price / item_doc.valuation_rate),2);		
				if factor == 100:
					frappe.throw(__("Margin cannot be 100%"))
				calc_margin = round((100 - (100 / factor)),2)
				item_doc.standard_rate = price_doc.price_list_rate
				item_doc.x_margin = calc_margin
				item_doc.save()
	else:
		if self.x_margin != 0:
			frappe.throw(_("Margin can only be set for non-group nodes"))